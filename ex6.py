# -- coding: utf-8 --

"""
Created on Fri Mar 13 15:10:17 2020

@author: edouard.roger
"""

import unittest

from utils.operations import SimpleCalculator


class Test(unittest.TestCase):
    def test_addition(self):

        CALC = SimpleCalculator(10, 2)
        SOMME = CALC.sum()
        self.assertEqual(SOMME, 12)

        CALC = SimpleCalculator(2, "10")
        SOMME = CALC.sum()
        self.assertEqual(SOMME, "ERROR")

        CALC = SimpleCalculator(-2, -10)
        SOMME = CALC.sum()
        self.assertEqual(SOMME, -12)
        self.assertNotEqual(SOMME, 12)

    def test_substration(self):

        CALC = SimpleCalculator(10, 2)
        SOUSTRACTION = CALC.subtract()
        self.assertEqual(SOUSTRACTION, 8)

        CALC = SimpleCalculator(2, "10")
        SOUSTRACTION = CALC.subtract()
        self.assertEqual(SOUSTRACTION, "ERROR")

        CALC = SimpleCalculator(-2, -10)
        SOUSTRACTION = CALC.subtract()
        self.assertEqual(SOUSTRACTION, 8)
        self.assertNotEqual(SOUSTRACTION, -12)

    def test_multiplication(self):

        CALC = SimpleCalculator(10, 2)
        MULTIPLICATION = CALC.multiply()
        self.assertEqual(MULTIPLICATION, 20)

        CALC = SimpleCalculator(2, "10")
        MULTIPLICATION = CALC.subtract()
        self.assertEqual(MULTIPLICATION, "ERROR")

        CALC = SimpleCalculator(-2, -10)
        MULTIPLICATION = CALC.multiply()
        self.assertEqual(MULTIPLICATION, 20)
        self.assertNotEqual(MULTIPLICATION, -20)

    def test_division(self):

        CALC = SimpleCalculator(10, 2)
        DIVISION = CALC.divide()
        self.assertEqual(DIVISION, 5)

        CALC = SimpleCalculator(2, "10")
        DIVISION = CALC.divide()
        self.assertEqual(DIVISION, "ERROR")

        CALC = SimpleCalculator(-10, -2)
        DIVISION = CALC.divide()
        self.assertEqual(DIVISION, 5)
        self.assertNotEqual(DIVISION, -5)

        with self.assertRaises(ZeroDivisionError):
            CALC.divide(10, 0)


unittest.main()
